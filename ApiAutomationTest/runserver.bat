@echo off
rem encodig=GB2312

title 正在运行
echo **********************************************************************
echo *************************【web服务启动】******************************
set pypath="%cd%\venv\Scripts\python.exe"
echo %pypath%
if exist %pypath% (
    echo *************************【使用项目环境下Python解释器执行】***********
    %pypath% webserver.py
) else (
    echo *************************【使用系统环境变量下Python解释器执行】*******
    python webserver.py
)
title 执行结束
pause