# coding=utf-8

from flask import send_file
from flask_login import current_user, login_required
import os

from app.utils import bp
from app.config import attachment_dir


@bp.route("/download/<string:filename>", methods=['GET'])
@login_required
def download_file(filename):
    user_id = current_user.id
    file_path = os.path.join(attachment_dir, str(user_id), filename)
    return send_file(file_path, as_attachment=True)
