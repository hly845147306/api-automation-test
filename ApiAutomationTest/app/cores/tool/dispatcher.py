# coding=utf-8

from app.cores.dictionaries import DISPATCHER_TYPE
from app.cores.dispatcher import AbstractToolDispatcher
from app.cores.tool.script import Script
from app.cores.tool.timer import Timer
from app.cores.tool.variable_definition import VariableDefinition
from app.cores.tool.http_header_manager import HTTPHeaderManager
from app.cores.tool.http_cookie_manager import HTTPCookieManager


class ScriptToolDispatcher(AbstractToolDispatcher):
    def __init__(self, tool, dispatcher_type=DISPATCHER_TYPE.DEBUG, logger=None, dispatcher=None, project_id=None):
        """
        :param tool: 单个Script Tool工具组件
        :type tool: Tool
        :param dispatcher_type: 标识构建是通过单独组件调试(DISPATCHER_TYPE.DEBUG)还是通过模块/项目构建测试(DISPATCHER_TYPE.BUILD)
        :type dispatcher_type: str
        :param logger: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度日志
        :type logger: DispatcherLogger
        :param dispatcher: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度数据
        :type dispatcher: Dispatcher
        :param project_id: 工具所属的项目id
        :type project_id: int
        """
        super().__init__(tool=tool, dispatcher_type=dispatcher_type, logger=logger, dispatcher=dispatcher)

        self.project_id = project_id
        # 脚本执行日志
        self.log_text = ''

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        self.log_text = Script(tool=self.tool, dispatcher_logger=self.dispatcher_logger, project_id=self.project_id).exec_tool()

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()
        return {
            'log_text': self.log_text,
        }


class TimerToolDispatcher(AbstractToolDispatcher):
    def __init__(self, tool, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None, dispatcher=None):
        """
        :param tool: 单个Timer Tool工具组件
        :type tool: Tool
        :param dispatcher_type: 标识构建是通过单独组件调试(DISPATCHER_TYPE.DEBUG)还是通过模块/项目构建测试(DISPATCHER_TYPE.BUILD)
        :type dispatcher_type: str
        :param logger: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度日志
        :type logger: DispatcherLogger
        :param dispatcher: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度数据
        :type dispatcher: Dispatcher
        """
        super().__init__(tool=tool, dispatcher_type=dispatcher_type, logger=logger, dispatcher=dispatcher)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        Timer(tool=self.tool, dispatcher_logger=self.dispatcher_logger).exec_tool()

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class VariableDefinitionToolDispatcher(AbstractToolDispatcher):
    def __init__(self, tool, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None, dispatcher=None):
        """
        :param tool: 单个VariableDefinition Tool工具组件
        :type tool: Tool
        :param dispatcher_type: 标识构建是通过单独组件调试(DISPATCHER_TYPE.DEBUG)还是通过模块/项目构建测试(DISPATCHER_TYPE.BUILD)
        :type dispatcher_type: str
        :param logger: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度日志
        :type logger: DispatcherLogger
        :param dispatcher: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度数据
        :type dispatcher: Dispatcher
        """
        super().__init__(tool=tool, dispatcher_type=dispatcher_type, logger=logger, dispatcher=dispatcher)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        VariableDefinition(tool=self.tool, dispatcher_logger=self.dispatcher_logger).exec_tool()

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class HTTPHeaderManagerToolDispatcher(AbstractToolDispatcher):
    def __init__(self, tool, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None, dispatcher=None):
        """
        :param tool: 单个HTTPHeaderManager Tool工具组件
        :type tool: Tool
        :param dispatcher_type: 标识构建是通过单独组件调试(DISPATCHER_TYPE.DEBUG)还是通过模块/项目构建测试(DISPATCHER_TYPE.BUILD)
        :type dispatcher_type: str
        :param logger: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度日志
        :type logger: DispatcherLogger
        :param dispatcher: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度数据
        :type dispatcher: Dispatcher
        """
        super().__init__(tool=tool, dispatcher_type=dispatcher_type, logger=logger, dispatcher=dispatcher)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        HTTPHeaderManager(tool=self.tool, dispatcher_logger=self.dispatcher_logger).exec_tool()

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class HTTPCookieManagerToolDispatcher(AbstractToolDispatcher):
    def __init__(self, tool, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None, dispatcher=None):
        """
        :param tool: 单个HTTPCookieManager Tool工具组件
        :type tool: Tool
        :param dispatcher_type: 标识构建是通过单独组件调试(DISPATCHER_TYPE.DEBUG)还是通过模块/项目构建测试(DISPATCHER_TYPE.BUILD)
        :type dispatcher_type: str
        :param logger: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度日志
        :type logger: DispatcherLogger
        :param dispatcher: 当dispatcher_type为DISPATCHER_TYPE.BUILD时，需要传入调度数据
        :type dispatcher: Dispatcher
        """
        super().__init__(tool=tool, dispatcher_type=dispatcher_type, logger=logger, dispatcher=dispatcher)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        HTTPCookieManager(tool=self.tool, dispatcher_logger=self.dispatcher_logger).exec_tool()

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()
