# coding=utf-8
from requests.cookies import RequestsCookieJar

from app.cores.case.http.http_cookie_pool_manager import HTTPCookiePoolManager
from app.cores.dictionaries import DISPATCHER_TYPE


class HTTPCookieManager:

    def __init__(self, tool, dispatcher_logger):
        """
        初始化HTTPCookieManager工具对象
        :param tool: 工具
        :type tool: Tool
        :param dispatcher_logger: 日志
        :type dispatcher_logger: DispatcherLogger
        """
        self.http_cookie_manager_tool = tool.specific_tool
        self.dispatcher_logger = dispatcher_logger

    def exec_tool(self):
        self.dispatcher_logger.logger.info('[HTTPCookie管理器][执行]')
        rcj = HTTPCookiePoolManager.get_request_cookie_jar(type=DISPATCHER_TYPE.BUILD)
        new_rcj = RequestsCookieJar()
        for row in self.http_cookie_manager_tool.http_cookie_manager_list:
            new_rcj.set(name=row.name_,
                        value=row.value_,
                        domain=row.domain_,
                        path=row.path_,
                        secure=row.secure)
        HTTPCookiePoolManager.update_request_cookie_jar(old_rcj=rcj, new_rcj=new_rcj)
