# coding=utf-8
from app.cores.variable import Variable

from flask import session


class VariableDefinition:

    def __init__(self, tool, dispatcher_logger, project_id=None):
        """
        初始化变量定义工具对象
        :param tool: 工具
        :type tool: Tool
        :param dispatcher_logger: 日志
        :type dispatcher_logger: DispatcherLogger
        :param project_id: 工具所在项目id
        :type project_id: int
        """
        self.variable_definition_tool = tool.specific_tool
        self.dispatcher_logger = dispatcher_logger
        if project_id is None:
            self.project_id = session.get('project_id')
        else:
            self.project_id = project_id

    def exec_tool(self):
        self.dispatcher_logger.logger.info('[变量定义][执行]')
        variables = Variable.get_project_variable(project_id=self.project_id)
        for row in self.variable_definition_tool.variable_definition_list:
            variables[row.name_] = row.value_
