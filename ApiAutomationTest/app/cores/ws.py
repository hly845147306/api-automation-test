# coding=utf-8

from flask_login import current_user

from app.extensions import socketio
from app.models import User


class SocketIOEvent:
    DISPATCHER_RESULT = 'DISPATCHER_RESULT'  # 测试执行结果
    DISPATCHER_BEGIN = 'DISPATCHER_BEGIN'    # 调度执行开始
    DISPATCHER_END = 'DISPATCHER_END'        # 调度执行结束


def emit_dispatcher_result(id, type, result):
    """
    测试执行结果
    :param id: 组件id
    :param type: 组件类型
    :param result: 结果
    :return:
    """
    user_id = current_user.id if current_user.is_authenticated else 0
    socketio.emit(
        SocketIOEvent.DISPATCHER_RESULT,
        dict(
            id=str(id),
            type=str(type),
            result=str(result),
        ),
        namespace='/user/' + str(user_id),
    )


def emit_dispatcher_start(id, type, report_id):
    """
    调度执行开始
    :param id:调度id
    :param type:调度类型 project/module
    :param report_id: 报告id
    """
    user_id = current_user.id if current_user.is_authenticated else 0
    socketio.emit(
        SocketIOEvent.DISPATCHER_BEGIN,
        dict(
            id=str(id),
            type=str(type).lower(),
            report_id=str(report_id),
        ),
        namespace='/user/' + str(user_id),
    )


def emit_dispatcher_end(id, type, end_type):
    """
    调度执行结束
    :param id:调度id
    :param type:调度类型 project/module
    :param end_type: 结束类型 DISPATCHER_END_TYPE
    """
    user_id = current_user.id if current_user.is_authenticated else 0
    socketio.emit(
        SocketIOEvent.DISPATCHER_END,
        dict(
            id=str(id),
            type=str(type).lower(),
            end_type=end_type,
        ),
        namespace='/user/' + str(user_id),
    )

# def test_message(message):
#     print('收到：%s' % message)
#     socketio.emit(
#         'new response',
#         '服务端的消息推送给用户: %s' % current_user.id,
#         namespace='/user/' + str(current_user.id)
#     )
#
#
# def register_user_socket(user_id):
#     socketio.on(
#         message='new message',
#         namespace='/user/' + str(user_id),
#     )(test_message)


def register_all_user_socket():
    users = User.query.all()
    user_id_list = [user.id for user in users]
    for user_id in user_id_list:
        # register_user_socket(user_id=user_id)  # 目前没有注册服务端socket事件监听的需求
        pass

# @socketio.on('connect')
# def test_connect():
#     print('当前用户%s已连接' % current_user.id)
#
#
# @socketio.on('disconnect')
# def test_disconnect():
#     print('用户%s断开连接' % current_user.id)
