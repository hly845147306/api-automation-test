# coding=utf-8

from typing import Dict
from flask import session

from app.extensions import session_id_manager
from app.cores.dictionaries import DISPATCHER_TRIGGER_TYPE


class HTTPHeaderPoolManager:

    """
    HTTPHeaderPool = {
        session_id: {
            project_id: {
                key: value,
                key1: value1,
                ...
            },
            project_id2: {
                ...
            }
        },
        session_id2:{
            ...
        },
        ...
    }

    ScheduleHTTPHeaderPool = {
        project_id: {
            key: value,
            key1: value1,
            ...
        },
        project_id2: {
            ...
        },
        ...
    }
    """
    # HTTP请求头数据池
    HTTPHeaderPool = {}

    # 定时任务HTTP请求头数据池
    ScheduleHTTPHeaderPool = {}

    @classmethod
    def get_http_header(cls, project_id):
        """
        获取项目中HTTP请求头数据
        :param project_id: 项目id
        :type project_id: int
        :return: 项目定义的HTTP请求头数据
        :rtype: Dict
        """
        cls._set_default_http_header(project_id=project_id)
        if session.get('dispatcher_trigger_type') == DISPATCHER_TRIGGER_TYPE.BY_SCHEDULE:
            project_id = session.get('project_id')
            if project_id:
                return cls.ScheduleHTTPHeaderPool.get(project_id)
        else:
            session_id = session_id_manager.get_session_id()
            return cls.HTTPHeaderPool.get(session_id).get(project_id)

    @classmethod
    def set_http_header(cls, project_id, key, value):
        """
        设置HTTP请求头
        :param project_id: 项目id
        :type project_id: int
        :param key: 变量名
        :type key: str
        :param value: 变量值
        :type value: str
        """
        cls._set_default_http_header(project_id=project_id)
        if session.get('dispatcher_trigger_type') == DISPATCHER_TRIGGER_TYPE.BY_SCHEDULE:
            project_id = session.get('project_id')
            if project_id:
                cls.ScheduleHTTPHeaderPool.get(project_id).update({
                    key: value
                })
        else:
            session_id = session_id_manager.get_session_id()
            cls.HTTPHeaderPool.get(session_id).get(project_id).update({
                key: value
            })

    @classmethod
    def _set_default_http_header(cls, project_id):
        """当RequestHeaderPool为空时，为其设置默认值，避免取出值为None"""
        if session.get('dispatcher_trigger_type') == DISPATCHER_TRIGGER_TYPE.BY_SCHEDULE:
            project_id = session.get('project_id')
            if project_id:
                project_var = cls.ScheduleHTTPHeaderPool.get(project_id)
                if project_var is None:
                    cls.ScheduleHTTPHeaderPool.update({
                        project_id: {}
                    })
        else:
            session_id = session_id_manager.get_session_id()
            session_var = cls.HTTPHeaderPool.get(session_id)
            if session_var is None:
                cls.HTTPHeaderPool.update({
                    session_id: {
                        project_id: {}
                    }
                })
            else:
                project_var = cls.HTTPHeaderPool.get(session_id).get(project_id)
                if project_var is None:
                    cls.HTTPHeaderPool.get(session_id).update({
                        project_id: {}
                })
