# coding=utf-8

import time
from flask import session

from app.cores.variable import Variable


class DebugRequest:

    def __init__(self, project_variable):
        """
        :param project_variable: 是否展示项目变量
        :type project_variable: bool
        """
        self.project_variable = project_variable
        # 请求与响应
        self.response_headers = None  # type: dict
        self.response_body = None  # type: dict
        self.request_headers = None  # type: dict
        self.request_body = None  # type: dict
        self.vars = []
        # 请求响应耗时 ms
        self.elapsed_time = 0

    def send(self):
        # send request
        _start_clock = time.time()
        if self.project_variable:
            self.vars = Variable.get_project_variable(project_id=session.get('project_id'))
        # recv response
        _end_clock = time.time()
        self.elapsed_time = int((_end_clock - _start_clock) * 1000) + 1  # 执行过快导致结果耗时为0，这里特殊处理+1ms进行展示
        self._handle_response()

    def _handle_response(self):
        """处理应答解析拿到 应答体 应答头 请求体 请求头"""
        self._handle_response_headers()
        self._handle_request_body()
        self._handle_request_headers()
        self._handle_response_body()

    def _handle_response_headers(self):
        self.response_headers = {
            'project_variable': self.project_variable,
            'elapsed_time': self.elapsed_time,
        }

    def _handle_request_body(self):
        if self.project_variable:
            exec_ = "Variable.get_project_variable(project_id=session.get('project_id)')"
        else:
            exec_ = ''
        self.request_body = {
            'project_variable': self.project_variable,
            'exec': exec_,
        }

    def _handle_request_headers(self):
        self.request_headers = {
            'project_variable': self.project_variable,
        }

    def _handle_response_body(self):
        self.response_body = {
            'vars': self.vars,
        }


def make_request(project_variable):
    """
    :param project_variable: 是否展示项目变量
    """
    return DebugRequest(project_variable=project_variable)


if __name__ == '__main__':
    pass
