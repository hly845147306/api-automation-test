
from flask import Blueprint


bp = Blueprint("logic_controller", __name__)

from app.routes.logic_controller import routes
