# coding=utf-8

from flask import render_template
from flask_login import login_required

from app.routes.project import bp


@bp.route('/')
@login_required
def project():
    return render_template('project/project.html')
