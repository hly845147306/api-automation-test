
from flask import Blueprint


bp = Blueprint("case", __name__)

from app.routes.case import routes
