# coding=utf-8

from flask import render_template, abort
from flask_login import login_required

from app.routes.case import bp
from app.models import Case
from app.cores.dictionaries import STATUS, CASE_TYPE


@bp.route('/<string:id>')  # TODO string 改为 int
@login_required
def case(id):
    if int(id) > 0:
        # 从数据库匹配案例
        case = Case.query.filter_by(id=id).first()
        if (case is None) or (case.status != STATUS.NORMAL):
            abort(404)
        if case.case_type == CASE_TYPE.HTTP:
            return render_template('case/http/http.html', case=case)
        elif case.case_type == CASE_TYPE.SSH:
            return render_template('case/ssh.html', case=case)
        elif case.case_type == CASE_TYPE.SQL:
            return render_template('case/sql.html', case=case)
        elif case.case_type == CASE_TYPE.DEBUG:
            return render_template('case/debug.html', case=case)
        else:
            abort(404)
    else:
        abort(404)
