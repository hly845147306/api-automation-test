
from flask import Blueprint


bp = Blueprint("report", __name__)

from app.routes.report import routes
