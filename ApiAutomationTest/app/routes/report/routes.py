# coding=utf-8

from flask import render_template, abort
from flask_login import login_required

from app.routes.report import bp
from app.cores.dictionaries import ELEMENT_TYPE, REPORT_RESULT


@bp.route('/')
@login_required
def report():
    return render_template('report/report.html')


@bp.route('/detail/<int:report_id>')
@login_required
def report_detail(report_id):
    from app.models import Report
    report = Report.query.filter_by(id=report_id).first()
    if report is None:
        abort(404)
    dispatcher = report.dispatcher
    report_case_data = report.report_case_data
    success_count = 0
    failure_count = 0
    error_count = 0
    skip_count = 0
    case_count = 0
    for data in report_case_data:
        case_count += 1
        if data.result == REPORT_RESULT.SUCCESS:
            success_count += 1
        elif data.result == REPORT_RESULT.FAILURE:
            failure_count += 1
        elif data.result == REPORT_RESULT.ERROR:
            error_count += 1
        elif data.result == REPORT_RESULT.SKIP:
            skip_count += 1
    echart_pie_result_data = [
        {'name': REPORT_RESULT.SUCCESS, 'value': success_count},
        {'name': REPORT_RESULT.FAILURE, 'value': failure_count},
        {'name': REPORT_RESULT.ERROR, 'value': error_count},
        {'name': REPORT_RESULT.SKIP, 'value': skip_count},
    ]
    return render_template('report/report_detail.html', dispatcher=dispatcher, report=report, ELEMENT_TYPE=ELEMENT_TYPE,
                           echart_pie_result_data=echart_pie_result_data, case_count=case_count,
                           failure_count=failure_count, error_count=error_count, success_count=success_count,
                           skip_count=skip_count)
