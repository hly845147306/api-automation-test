# coding=utf-8

from flask import Blueprint


bp = Blueprint("error", __name__)

from app.routes.error import routes
