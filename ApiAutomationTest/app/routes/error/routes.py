# coding=utf-8

from flask import render_template
from flask_wtf.csrf import CSRFError

from app.extensions import db
from app.routes.error import bp
from app.utils.util import save_request_session_info


@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('/error/404.html'), 404


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    save_request_session_info()
    return render_template('/error/500.html'), 500


@bp.app_errorhandler(CSRFError)
def csrf_error(error):
    save_request_session_info()
    return render_template('/error/error.html', msg='csrf_token异常: %s' % error.description), 400


@bp.app_errorhandler(400)
def bad_request_error(error):
    save_request_session_info()
    return render_template('/error/error.html', msg='错误请求: %s' % error.description), 400
