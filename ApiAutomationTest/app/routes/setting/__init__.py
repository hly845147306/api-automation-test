# coding=utf-8

from flask import Blueprint


bp = Blueprint("setting", __name__)

from app.routes.setting import routes
