# coding=utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectMultipleField, SelectField
from wtforms.validators import DataRequired, Email, EqualTo, Length
from wtforms.validators import DataRequired, Regexp, ValidationError


def required_when_send_mail(form, field):
    if (form.whether_send_email.data == 'true') and (not field.data):
        raise ValidationError('当发送邮件时，此项不能为空')


class EmailSettingForm(FlaskForm):
    whether_send_email = SelectField(label='是否发送邮件',
                                     choices=[('false', '否'), ('true', '是')],
                                     render_kw={})
    whether_gen_report = SelectField(label='是否发送生成报告',
                                     choices=[('false', '否'), ('true', '是')],
                                     render_kw={})
    email_title = StringField(label='邮件主题',
                              validators=[required_when_send_mail],
                              render_kw={})
    email_text = StringField(label='正文内容',
                             validators=[required_when_send_mail],
                             render_kw={})
    receiver_address = StringField(label='收件地址',
                                   validators=[required_when_send_mail],
                                   render_kw={})
    submit = SubmitField('提交')
