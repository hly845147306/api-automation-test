# coding=utf-8

from flask import current_app, render_template

from app.models import User
from app.email import send_email


def send_email_authentication(email: str):
    token = User.get_email_token(email=email)
    send_email(
        subject="[ApiAutomationTest] 邮箱注册认证",
        sender=current_app.config['ADMINS'][0],
        recipients=[email],
        text_body=render_template("email/email_authentication.txt", token=token),
        html_body=render_template("email/email_authentication.html", token=token)
    )


def send_email_reset_password(email: str):
    token = User.get_email_token(email=email)
    send_email(
        subject="[ApiAutomationTest] 密码重置",
        sender=current_app.config['ADMINS'][0],
        recipients=[email],
        text_body=render_template("email/reset_password.txt", token=token),
        html_body=render_template("email/reset_password.html", token=token)
    )
