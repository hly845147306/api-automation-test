# coding=utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectMultipleField
from wtforms.validators import DataRequired, Email, EqualTo, Length


class LoginForm(FlaskForm):
    username = StringField(label="用户名", validators=[DataRequired(message="用户名不能为空"), Length(5, message='长度过短')])
    password = PasswordField(label="密码", validators=[DataRequired(message="密码不能为空")])
    submit = SubmitField(label="登录")


class RegistrationForm(FlaskForm):
    username = StringField(label='用户名', validators=[DataRequired(message="用户名不能为空")])
    email = StringField(label='邮箱',
                        validators=[DataRequired(message="邮箱不能为空"), Email()],
                        render_kw={})
    password = PasswordField(label='密码', validators=[DataRequired(message="密码不能为空")])
    password2 = PasswordField(label='重复输入密码',
                              validators=[DataRequired(message="密码不能为空"),
                                          EqualTo(fieldname='password', message="两次密码输入不一致")])
    submit = SubmitField(label='注册')


class EmailAuthenticationForm(FlaskForm):
    email = StringField(label="请输入有效邮箱",
                        validators=[DataRequired(message="邮箱不能为空"), Email(message="请输入正确格式邮箱地址")])
    submit = SubmitField(label="点击认证")


class ResetPasswordForm(FlaskForm):
    email = StringField(label='请输入注册邮箱, 将向此邮箱发送密码重置邮件',
                        validators=[DataRequired(message='邮箱不能为空')])
    submit = SubmitField(label='确定')


class ResetForm(FlaskForm):
    email = StringField(label='邮箱',
                        validators=[DataRequired(message='邮箱不能为空')],
                        render_kw={})
    usernames = SelectMultipleField(label='可选一个或多个用户重置',
                                    validators=[DataRequired('用户名不能为空')],
                                    choices=[])
    password = PasswordField(label='请输入新的密码',
                             validators=[DataRequired(message='密码不能为空')])
    password2 = PasswordField(label='重复输入密码',
                              validators=[DataRequired(message='密码不能为空'),
                                          EqualTo(fieldname='password', message="两次密码输入不一致")])
    submit = SubmitField(label='确定')
