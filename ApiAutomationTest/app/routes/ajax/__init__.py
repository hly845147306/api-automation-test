# coding=utf-8

from flask import Blueprint


bp = Blueprint("ajax", __name__)

from app.routes.ajax import routes
