// $(document).ready(function () {
//
//     // 输入框格式化
//     let maskOption = {
//         translation: {
//             'Z': {
//                 pattern: /./, optional: true
//             }
//         }
//     };
//     $('#input-domain').mask('Z'.repeat(200), maskOption);
//     $('#input-path').mask('Z'.repeat(200), maskOption);
//     $("#input-port").inputmask({regex: "\\d{0,10}"});
//     $("#input-content-encoding").inputmask({regex: "[a-zA-z\\-\\d]*"});
//
//     // 渲染参数表格
//     renderParameterTable();
//     // 渲染文件上传表格
//     renderFileUploadTable();
//     // 渲染期望表格
//     renderExpectationTable();
//     // 支持上下分割
//     renderSplitter([50, 100]);
//
//     // 消息体数据
//     ace_message_body_editor = ace.edit("div-ace-message-body");
//     ace_message_body_editor.setTheme("ace/theme/eclipse");
//     ace_message_body_editor.session.setMode("ace/mode/json");
// });
//
// let table_parameter = null;  // 请求参数表格
// let table_file_upload = null;  // 文件上传表格
// let ace_message_body_editor = null;  // 消息体数据格式化编辑器
//
// // 界面元素
// let $select_protocol = $("#select-protocol");
// let $input_domain = $("#input-domain");
// let $input_port = $("#input-port");
// let $input_path = $("#input-path");
// let $input_content_encoding = $("#input-content-encoding");
// let $div_table_parameter_spinner = $('#div-table-parameter-spinner');
// let $div_table_file_upload_spinner = $('#div-table-file-upload-spinner');
//
// // 渲染参数表格
// function renderParameterTable() {
//     const container_table_parameter = document.getElementById('table-parameter');
//     const option_table_parameter = {
//         data: data_parameters,
//         rowHeaders: true,
//         // colHeaders: true,
//         licenseKey: 'non-commercial-and-evaluation',
//         // 表格宽度
//         // width: '95vw',  // 不指定宽度使其自适应容器
//         // 拉伸方式
//         stretchH: 'all',
//         // tab键自动换行切换
//         autoWrapRow: true,
//         height: '50vh',
//         // 最大行数
//         maxRows: 99,
//         // 允许手工移动行或列
//         manualRowResize: true,
//         manualColumnResize: true,
//         // 列名
//         colHeaders: [
//             '名称',
//             '值',
//             '编码?',
//             '内容类型',
//             '包含等于?',
//         ],
//         // 为列设置默认值
//         dataSchema: {
//             name: '',
//             value: '',
//             url_encode: false,
//             content_type: 'text/plain',
//             include_equals: true,
//         },
//         // 设置列数据类型
//         columns: [
//             {
//                 data: 'name'
//             },
//             {
//                 data: 'value',
//             },
//             {
//                 data: 'url_encode',
//                 type: 'checkbox'
//             },
//             {
//                 data: 'content_type'
//             },
//             {
//                 data: 'include_equals',
//                 type: 'checkbox',
//             },
//         ],
//         // 列宽比例
//         colWidths: [2, 10, 1, 2, 2],
//         manualRowMove: true,
//         manualColumnMove: false,
//         // 右键菜单
//         contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
//         // 列是否支持过滤
//         filters: false,
//         // 下拉菜单
//         dropdownMenu: ['make_read_only', '---------', 'alignment'],
//         // 语言
//         language: 'zh-CN',
//         // 是否允许无效数据 默认 true
//         allowInvalid: false,
//         afterPaste: afterPasteHookForTableParam,
//         afterLoadData: afterLoadDataHookForTableParam,
//     };
//     table_parameter = new Handsontable(container_table_parameter, option_table_parameter);
//
//     // 解决复制粘贴后checkBox中的值由布尔类型变为字符串的问题
//     function afterPasteHookForTableParam() {
//         let data = table_parameter.getSourceData();
//         for (let i = 0; i < data.length; ++i) {
//             let row = data[i];
//             if (row.url_encode === 'false'){
//                 row.url_encode = false;
//             }
//             if (row.url_encode === 'true'){
//                 row.url_encode = true;
//             }
//             if (row.include_equals === 'false'){
//                 row.include_equals = false;
//             }
//             if (row.include_equals === 'true'){
//                 row.include_equals = true;
//             }
//         }
//         table_parameter.loadData(data);
//     }
//
//     // 数据加载完毕后隐藏spinner
//     function afterLoadDataHookForTableParam() {
//         $div_table_parameter_spinner.css('display', 'none');
//     }
// }
//
// // 渲染文件上传表格
// function renderFileUploadTable() {
//     const container_table_file_upload = document.getElementById('table-file-upload');
//     const option_table_file_upload = {
//         data: data_file_upload,
//         rowHeaders: true,
//         // colHeaders: true,
//         licenseKey: 'non-commercial-and-evaluation',
//         // 表格宽度
//         // width: '95vw',  // 不指定宽度使其自适应容器
//         // 拉伸方式
//         stretchH: 'all',
//         // tab键自动换行切换
//         autoWrapRow: true,
//         height: '50vh',
//         // 最大行数
//         maxRows: 99,
//         // 允许手工移动行或列
//         manualRowResize: true,
//         manualColumnResize: true,
//         // 列名
//         colHeaders: [
//             '名称',
//             '值',
//         ],
//         // 为列设置默认值
//         dataSchema: {
//             name: '',
//             value: '',
//         },
//         // 设置列数据类型
//         columns: [
//             {
//                 data: 'name'
//             },
//             {
//                 data: 'value',
//             },
//         ],
//         // 列宽比例
//         colWidths: [1, 5],
//         manualRowMove: true,
//         manualColumnMove: false,
//         // 右键菜单
//         contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
//         // 列是否支持过滤
//         filters: false,
//         // 下拉菜单
//         dropdownMenu: ['make_read_only', '---------', 'alignment'],
//         // 语言
//         language: 'zh-CN',
//         // 是否允许无效数据 默认 true
//         allowInvalid: false,
//         afterLoadData: afterLoadDataHookForTableLoadFile,
//     };
//     table_file_upload = new Handsontable(container_table_file_upload, option_table_file_upload);
//
//     // 数据加载完毕后隐藏spinner
//     function afterLoadDataHookForTableLoadFile() {
//         $div_table_file_upload_spinner.css('display', 'none');
//     }
// }
//
// // 用例保存
// function saveCase() {
//     let id = $btn_case_save.data("case-id").toString();
//     let name = $input_case_name.val();
//     let description = $input_case_description.val();
//     let protocol = $select_protocol.find("option:selected").text();
//     let domain = $input_domain.val();
//     let port = $input_port.val();
//     let method = $select_method.find("option:selected").text();
//     let path = $input_path.val();
//     let content_encoding = $input_content_encoding.val();
//     let message_body_json_str = ace_message_body_editor.session.getValue();
//     let preprocessor_script_py_str = ace_preprocessor_script_editor.session.getValue();
//     let postprocessor_script_py_str = ace_postprocessor_script_editor.session.getValue();
//
//     let expectation_logic = '';
//     if ($radio_expectation_logic_and.is(':checked')){
//         expectation_logic = '与';
//     }
//     if ($radio_expectation_logic_or.is(':checked')){
//         expectation_logic = '或';
//     }
//
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/case/http/save',
//         data: {
//             id: id,
//             name: name,
//             description: description,
//             protocol: protocol,
//             domain: domain,
//             port: port,
//             method: method,
//             path: path,
//             content_encoding: content_encoding,
//             expectation_logic: expectation_logic,
//             param_json: JSON.stringify(table_parameter.getSourceData()),
//             file_upload_json: JSON.stringify(table_file_upload.getSourceData()),
//             expectation_json: JSON.stringify(table_expectation.getSourceData()),
//             message_body_json: message_body_json_str,
//             preprocessor_script: preprocessor_script_py_str,
//             postprocessor_script: postprocessor_script_py_str,
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message("保存案例成功", "success")
//             }else{
//                 message("保存案例失败: " + data.error_msg, "error");
//             }
//         },
//     });
// }
//

function getHTTPCaseElement(case_id, case_type) {

    let element = getBaseCaseElement(case_id, case_type);
    let baseCaseInit = element.init;  // baseCase中定义的初始化函数
    let baseCaseRenderSplitter = element.renderSplitter;  // 分隔栏渲染
    let baseMark = element.mark;  // 关键词搜索

    // 界面元素
    element.dom.$select_protocol = $(`#select-protocol-${case_id}`);
    element.dom.$input_domain = $(`#input-domain-${case_id}`);
    element.dom.$input_port = $(`#input-port-${case_id}`);
    element.dom.$input_path = $(`#input-path-${case_id}`);
    element.dom.$input_content_encoding = $(`#input-content-encoding-${case_id}`);
    element.dom.$input_content_type = $(`#input-content-type-${case_id}`);
    element.dom.$div_table_parameter_spinner = $(`#div-table-parameter-spinner-${case_id}`);
    element.dom.$div_table_header_spinner = $(`#div-table-header-spinner-${case_id}`);
    element.dom.$div_table_file_upload_spinner = $(`#div-table-file-upload-spinner-${case_id}`);
    element.dom.$nav_link_param_tab = $(`#param-tab-${case_id}`);
    element.dom.$nav_link_header_tab = $(`#header-tab-${case_id}`);
    element.dom.$nav_link_file_upload_tab = $(`#file-upload-tab-${case_id}`);

    element.obj.table_parameter = null;  // 请求参数表格
    element.obj.table_header = null;  // 请求头表格
    element.obj.table_file_upload = null;  // 文件上传表格
    element.obj.ace_message_body_editor = null;  // 消息体数据格式化编辑器
    element.obj.splitterMinSize = [50, 70];  // splitter分割条最小长度

    element.init = function (data_expectations, data_parameters, data_headers, data_file_upload) {
        // 参数
        // data_expectations: 案例期望数据
        // data_parameters:   案例请求参数数据
        // data_file_upload:  案例文件上传参数数据

        // 支持上下分割
        // element.renderSplitter([50, 100]);
        // baseCase初始化
        baseCaseInit(data_expectations);
        // 输入框格式化
        maskInput();
        // 渲染文本编辑
        renderAceEditor();
        // 渲染参数表格
        renderParameterTable(data_parameters);
        // 渲染请求头表格
        renderHeaderTable(data_headers);
        // 渲染文件上传表格
        renderFileUploadTable(data_file_upload);
        // 获取CookieManager对象并初始化
        let cookieManager = getCookieManager(case_id);
        cookieManager.init();
        // 事件绑定
        eventBinding();
    };

    function maskInput() {
        // 输入框格式化
        let maskOption = {
            translation: {
                'Z': {
                    pattern: /./, optional: true
                }
            }
        };
        element.dom.$input_domain.mask('Z'.repeat(200), maskOption);
        element.dom.$input_path.mask('Z'.repeat(200), maskOption);
        element.dom.$input_port.inputmask({regex: "\\d{0,10}"});
        element.dom.$input_content_encoding.inputmask({regex: "[a-zA-z\\-\\d]*"});
    }

    // 渲染文本编辑
    function renderAceEditor() {
        // 消息体数据
        element.obj.ace_message_body_editor = ace.edit(`div-ace-message-body-${case_id}`);
        element.obj.ace_message_body_editor.setTheme("ace/theme/eclipse");
        element.obj.ace_message_body_editor.session.setMode("ace/mode/json");
    }

    // 事件绑定
    function eventBinding() {
        element.dom.$nav_link_param_tab.on('shown.bs.tab', clickParamTab);
        element.dom.$nav_link_header_tab.on('shown.bs.tab', clickHeaderTab);
        element.dom.$nav_link_file_upload_tab.on('shown.bs.tab', clickFileTab);

        function clickParamTab() {
            // 渲染ht
            $(`#table-parameter-${case_id}`).handsontable('getInstance').render();
        }

        function clickHeaderTab() {
            // 渲染ht
            $(`#table-header-${case_id}`).handsontable('getInstance').render();
        }

        function clickFileTab() {
            // 渲染ht
            $(`#table-file-upload-${case_id}`).handsontable('getInstance').render();
        }
    }

    // 渲染参数表格
    function renderParameterTable(data_parameters) {
        // const container_table_parameter = document.getElementById(`table-parameter-${case_id}`);
        const option_table_parameter = {
            data: data_parameters,
            rowHeaders: true,
            // colHeaders: true,
            licenseKey: 'non-commercial-and-evaluation',
            // 表格宽度
            // width: '95vw',  // 不指定宽度使其自适应容器
            // 拉伸方式
            stretchH: 'all',
            // tab键自动换行切换
            autoWrapRow: true,
            height: '40vh',
            // 最大行数
            maxRows: 99,
            // 允许手工移动行或列
            manualRowResize: true,
            manualColumnResize: true,
            // 列名
            colHeaders: [
                '名称',
                '值',
                '编码?',
                '内容类型',
                '包含等于?',
            ],
            // 为列设置默认值
            dataSchema: {
                name: '',
                value: '',
                url_encode: false,
                content_type: 'text/plain',
                include_equals: true,
            },
            // 设置列数据类型
            columns: [
                {
                    data: 'name'
                },
                {
                    data: 'value',
                },
                {
                    data: 'url_encode',
                    type: 'checkbox'
                },
                {
                    data: 'content_type'
                },
                {
                    data: 'include_equals',
                    type: 'checkbox',
                },
            ],
            // 列宽比例
            colWidths: [2, 10, 1, 2, 2],
            manualRowMove: true,
            manualColumnMove: false,
            // 右键菜单
            contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
            // 列是否支持过滤
            filters: false,
            // 下拉菜单
            dropdownMenu: ['make_read_only', '---------', 'alignment'],
            // 语言
            language: 'zh-CN',
            // 是否允许无效数据 默认 true
            allowInvalid: false,
            afterPaste: afterPasteHookForTableParam,
            afterLoadData: afterLoadDataHookForTableParam,
        };
        // element.obj.table_parameter = new Handsontable(container_table_parameter, option_table_parameter);
        let $container = $(`#table-parameter-${case_id}`);
        element.obj.table_parameter = $container.handsontable(option_table_parameter).handsontable('getInstance');

        // 解决复制粘贴后checkBox中的值由布尔类型变为字符串的问题
        function afterPasteHookForTableParam() {
            let data = element.obj.table_parameter.getSourceData();
            for (let i = 0; i < data.length; ++i) {
                let row = data[i];
                if (row.url_encode === 'false'){
                    row.url_encode = false;
                }
                if (row.url_encode === 'true'){
                    row.url_encode = true;
                }
                if (row.include_equals === 'false'){
                    row.include_equals = false;
                }
                if (row.include_equals === 'true'){
                    row.include_equals = true;
                }
            }
            element.obj.table_parameter.loadData(data);
        }

        // 数据加载完毕后隐藏spinner
        function afterLoadDataHookForTableParam() {
            element.dom.$div_table_parameter_spinner.css('display', 'none');
        }
    }

    // 渲染请求头表格
    function renderHeaderTable(data_headers) {
        const option_table_header = {
            data: data_headers,
            rowHeaders: true,
            // colHeaders: true,
            licenseKey: 'non-commercial-and-evaluation',
            // 表格宽度
            // width: '95vw',  // 不指定宽度使其自适应容器
            // 拉伸方式
            stretchH: 'all',
            // tab键自动换行切换
            autoWrapRow: true,
            height: '40vh',
            // 最大行数
            maxRows: 99,
            // 允许手工移动行或列
            manualRowResize: true,
            manualColumnResize: true,
            // 列名
            colHeaders: [
                '名称',
                '值',
            ],
            // 为列设置默认值
            dataSchema: {
                name: '',
                value: '',
                url_encode: false,
                content_type: 'text/plain',
                include_equals: true,
            },
            // 设置列数据类型
            columns: [
                {
                    data: 'name'
                },
                {
                    data: 'value',
                },
            ],
            // 列宽比例
            colWidths: [1, 1],
            manualRowMove: true,
            manualColumnMove: false,
            // 右键菜单
            contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
            // 列是否支持过滤
            filters: false,
            // 下拉菜单
            dropdownMenu: ['make_read_only', '---------', 'alignment'],
            // 语言
            language: 'zh-CN',
            // 是否允许无效数据 默认 true
            allowInvalid: false,
            afterLoadData: afterLoadDataHookForTableParam,
        };
        let $container = $(`#table-header-${case_id}`);
        element.obj.table_header = $container.handsontable(option_table_header).handsontable('getInstance');

        // 数据加载完毕后隐藏spinner
        function afterLoadDataHookForTableParam() {
            element.dom.$div_table_header_spinner.css('display', 'none');
        }
    }

    // 渲染文件上传表格
    function renderFileUploadTable(data_file_upload) {
        // const container_table_file_upload = document.getElementById(`table-file-upload-${case_id}`);
        const option_table_file_upload = {
            data: data_file_upload,
            rowHeaders: true,
            // colHeaders: true,
            licenseKey: 'non-commercial-and-evaluation',
            // 表格宽度
            // width: '95vw',  // 不指定宽度使其自适应容器
            // 拉伸方式
            stretchH: 'all',
            // tab键自动换行切换
            autoWrapRow: true,
            height: '40vh',
            // 最大行数
            maxRows: 99,
            // 允许手工移动行或列
            manualRowResize: true,
            manualColumnResize: true,
            // 列名
            colHeaders: [
                '名称',
                '值',
            ],
            // 为列设置默认值
            dataSchema: {
                name: '',
                value: '',
            },
            // 设置列数据类型
            columns: [
                {
                    data: 'name'
                },
                {
                    data: 'value',
                },
            ],
            // 列宽比例
            colWidths: [1, 5],
            manualRowMove: true,
            manualColumnMove: false,
            // 右键菜单
            contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
            // 列是否支持过滤
            filters: false,
            // 下拉菜单
            dropdownMenu: ['make_read_only', '---------', 'alignment'],
            // 语言
            language: 'zh-CN',
            // 是否允许无效数据 默认 true
            allowInvalid: false,
            afterLoadData: afterLoadDataHookForTableLoadFile,
        };
        // element.obj.table_file_upload = new Handsontable(container_table_file_upload, option_table_file_upload);
        let $container = $(`#table-file-upload-${case_id}`);
        element.obj.table_file_upload = $container.handsontable(option_table_file_upload).handsontable('getInstance');

        // 数据加载完毕后隐藏spinner
        function afterLoadDataHookForTableLoadFile() {
            element.dom.$div_table_file_upload_spinner.css('display', 'none');
        }
    }

    // 用例保存
    element.saveCase = function() {
        let id = case_id;
        let name = element.dom.$input_case_name.val();
        let description = element.dom.$input_case_description.val();
        let protocol = element.dom.$select_protocol.find("option:selected").text();
        let domain = element.dom.$input_domain.val();
        let port = element.dom.$input_port.val();
        let method = element.dom.$select_method.find("option:selected").text();
        let path = element.dom.$input_path.val();
        let content_encoding = element.dom.$input_content_encoding.val();
        let message_body_json_str = element.obj.ace_message_body_editor.session.getValue();
        let preprocessor_script_py_str = element.obj.ace_preprocessor_script_editor.session.getValue();
        let postprocessor_script_py_str = element.obj.ace_postprocessor_script_editor.session.getValue();

        let expectation_logic = '';
        if (element.dom.$radio_expectation_logic_and.is(':checked')){
            expectation_logic = '与';
        }
        if (element.dom.$radio_expectation_logic_or.is(':checked')){
            expectation_logic = '或';
        }
        let content_type = '';
        if (element.dom.$input_content_type.is(':checked')){
            content_type = 'multipart/form-data';
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/case/http/save',
            data: {
                id: id,
                name: name,
                description: description,
                protocol: protocol,
                domain: domain,
                port: port,
                method: method,
                path: path,
                content_encoding: content_encoding,
                expectation_logic: expectation_logic,
                content_type: content_type,
                param_json: JSON.stringify(element.obj.table_parameter.getSourceData()),
                header_json: JSON.stringify(element.obj.table_header.getSourceData()),
                file_upload_json: JSON.stringify(element.obj.table_file_upload.getSourceData()),
                expectation_json: JSON.stringify(element.obj.table_expectation.getSourceData()),
                message_body_json: message_body_json_str,
                preprocessor_script: preprocessor_script_py_str,
                postprocessor_script: postprocessor_script_py_str,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message("保存案例成功", "success")
                }else{
                    message("保存案例失败: " + data.error_msg, "error");
                }
            },
        });
    };

    // 分隔栏
    element.renderSplitter = function () {
        baseCaseRenderSplitter(element.obj.splitterMinSize);
    };

    // 关键词搜索
    element.mark = function (text) {
        let result = baseMark(text);
        if (result) return true;
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        let markFlag = false;
        // 服务器地址IP
        value = element.dom.$input_domain.val();
        if (pat.test(value)) return true;
        // 端口
        value = element.dom.$input_port.val();
        if (pat.test(value)) return true;
        // 路径
        value = element.dom.$input_path.val();
        if (pat.test(value)) return true;
        // 内容编码
        value = element.dom.$input_content_encoding.val();
        if (pat.test(value)) return true;
        // 参数表格
        markFlag = false;
        value = element.obj.table_parameter.getSourceData();
        $.each(value, function (index, row) {
            $.each(row, function (column, data) {
                if (pat.test(data)){
                    markFlag = true;
                    return false;
                }
            });
        });
        if (markFlag) return true;
        // 消息体数据
        value = element.obj.ace_message_body_editor.getValue();
        if (pat.test(value)) return true;
        // 文件上传表格
        markFlag = false;
        value = element.obj.table_file_upload.getSourceData();
        $.each(value, function (index, row) {
            $.each(row, function (column, data) {
                if (pat.test(data)){
                    markFlag = true;
                    return false;
                }
            });
        });
        if (markFlag) return true;
        // 未匹配到返回false
        return false;
    };

    return element;

}
