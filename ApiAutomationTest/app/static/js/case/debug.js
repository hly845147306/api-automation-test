function getDebugCaseElement(case_id, case_type) {
    let element = getBaseCaseElement(case_id, case_type);
    let baseCaseInit = element.init;  // baseCase中定义的初始化函数
    let baseCaseRenderSplitter = element.renderSplitter;  // 分隔栏渲染
    let baseMark = element.mark;  // 关键词搜索

    // 界面元素
    element.dom.$select_project_variable = $(`#select-project-variable-${case_id}`);

    element.obj.splitterMinSize = [50, 400];  // splitter分割条最小长度

    element.init = function (data_expectations) {
        // 参数
        // data_expectations: 案例期望数据

        // baseCase初始化
        baseCaseInit(data_expectations);
    };

    // 用例保存
    element.saveCase = function() {
        let id = case_id;
        let name = element.dom.$input_case_name.val();
        let description = element.dom.$input_case_description.val();
        let project_variable = element.dom.$select_project_variable.find("option:selected").text();
        let preprocessor_script_py_str = element.obj.ace_preprocessor_script_editor.session.getValue();
        let postprocessor_script_py_str = element.obj.ace_postprocessor_script_editor.session.getValue();

        let expectation_logic = '';
        if (element.dom.$radio_expectation_logic_and.is(':checked')){
            expectation_logic = '与';
        }
        if (element.dom.$radio_expectation_logic_or.is(':checked')){
            expectation_logic = '或';
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/case/debug/save',
            data: {
                id: id,
                name: name,
                description: description,
                project_variable: project_variable,
                expectation_logic: expectation_logic,
                expectation_json: JSON.stringify(element.obj.table_expectation.getSourceData()),
                preprocessor_script: preprocessor_script_py_str,
                postprocessor_script: postprocessor_script_py_str,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message("保存案例成功", "success")
                }else{
                    message("保存案例失败: " + data.error_msg, "error");
                }
            },
        });
    };

    // 分隔栏
    element.renderSplitter = function () {
        baseCaseRenderSplitter(element.obj.splitterMinSize);
    };

    // 关键词搜索
    element.mark = function (text) {
        let result = baseMark(text);
        if (result) return true;
        // 未匹配到返回false
        return false;
    };

    return element;
}
