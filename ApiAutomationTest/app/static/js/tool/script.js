function getScriptToolElement(tool_id) {
    let element = new Object;
    element.dom = new Object;
    element.obj = new Object;

    // 案例页面元素
    element.dom.$input_tool_name = $(`#input-tool-name-${tool_id}`);
    element.dom.$input_tool_description = $(`#input-tool-description-${tool_id}`);
    element.dom.$btn_tool_save = $(`#btn-tool-save-${tool_id}`);
    element.dom.$btn_tool_run = $(`#btn-tool-run-${tool_id}`);
    element.dom.$div_navigation_tool_name = $(`.tool-name[data-tool-id=${tool_id}]`);

    // 对象
    element.obj.ace_tool_script_editor = null;  // 脚本编辑器
    element.obj.ace_tool_log_editor = null;  // 日志

    // 初始化
    element.init = function() {
        // 事件绑定
        eventBinding();
        // 渲染文本编辑
        renderAceEditor();
    };

    // 事件绑定
    function eventBinding() {
        element.dom.$input_tool_name.on('change keyup', handleKeyUpAndChange);
        element.dom.$btn_tool_save.on('click', saveTool);
        element.dom.$btn_tool_run.on('click', runTool);

        function handleKeyUpAndChange(){
            let tool_name = element.dom.$input_tool_name.val();
            element.dom.$div_navigation_tool_name.children().text(tool_name);
        }
    }

    // 渲染文本编辑
    function renderAceEditor() {
        // 脚本
        element.obj.ace_tool_script_editor = ace.edit(`div-ace-tool-script-${tool_id}`);
        element.obj.ace_tool_script_editor.setTheme("ace/theme/eclipse");
        element.obj.ace_tool_script_editor.session.setMode("ace/mode/python");

        // 日志
        element.obj.ace_tool_log_editor = ace.edit(`div-ace-tool-log-${tool_id}`);
        element.obj.ace_tool_log_editor.setTheme("ace/theme/eclipse");
    }

    function saveTool() {
        let tool_name = element.dom.$input_tool_name.val();
        let tool_description = element.dom.$input_tool_description.val();
        let script = element.obj.ace_tool_script_editor.session.getValue();
        $.ajax({
            type: 'POST',
            url: '/ajax/tool/script/save',
            data: {
                tool_id: tool_id,
                tool_name: tool_name,
                tool_description: tool_description,
                script: script,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message('脚本保存成功', 'success');
                }else{
                    message("脚本保存失败: " + data.error_msg, "error");
                }
            }
        });
    }

    function runTool() {
        $.ajax({
            type: 'POST',
            url: '/ajax/tool/script/run',
            data: {
                tool_id: tool_id,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    element.obj.ace_tool_log_editor.session.setValue(data.log_text);
                    message('脚本执行成功', 'success');
                }else{
                    message("脚本执行失败: " + data.error_msg, "error");
                }
            }
        });
    }

    // 关键词查找
    element.mark = function (text) {
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        // 名称
        value = element.dom.$input_tool_name.val();
        if (pat.test(value)) return true;
        // 注释
        value = element.dom.$input_tool_description.val();
        if (pat.test(value)) return true;
        // 脚本
        value = element.obj.ace_tool_script_editor.getValue();
        if (pat.test(value)) return true;

        // 未匹配到返回false
        return false;
    };

    return element;
}