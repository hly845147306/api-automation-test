function getTimerToolElement(tool_id) {
    let element = new Object;
    element.dom = new Object;

    // 案例页面元素
    element.dom.$input_tool_name = $(`#input-tool-name-${tool_id}`);
    element.dom.$input_tool_description = $(`#input-tool-description-${tool_id}`);
    element.dom.$input_delay = $(`#input-delay-${tool_id}`);
    element.dom.$btn_tool_save = $(`#btn-tool-save-${tool_id}`);
    element.dom.$div_navigation_tool_name = $(`.tool-name[data-tool-id=${tool_id}]`);

    // 初始化
    element.init = function() {
        eventBinding();
    };

    // 事件绑定
    function eventBinding() {
        element.dom.$input_tool_name.on('change keyup', handleKeyUpAndChange);
        element.dom.$btn_tool_save.on('click', saveTool);

        function handleKeyUpAndChange(){
            let tool_name = element.dom.$input_tool_name.val();
            element.dom.$div_navigation_tool_name.children().text(tool_name);
        }
    }

    function saveTool() {
        let tool_name = element.dom.$input_tool_name.val();
        let tool_description = element.dom.$input_tool_description.val();
        let delay = element.dom.$input_delay.val();
        $.ajax({
            type: 'POST',
            url: '/ajax/tool/timer/save',
            data: {
                tool_id: tool_id,
                tool_name: tool_name,
                tool_description: tool_description,
                delay: delay,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message('定时器保存成功', 'success');
                }else{
                    message("定时器保存失败: " + data.error_msg, "error");
                }
            }
        });
    }

    // 关键词查找
    element.mark = function (text) {
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        // 名称
        value = element.dom.$input_tool_name.val();
        if (pat.test(value)) return true;
        // 注释
        value = element.dom.$input_tool_description.val();
        if (pat.test(value)) return true;
        // 暂停时间
        value = element.dom.$input_delay.val();
        if (pat.test(value)) return true;

        // 未匹配到返回false
        return false;
    };

    return element;
}