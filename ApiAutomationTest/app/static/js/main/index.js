$(document).ready(function () {

    // 渲染报告图表
    renderLastDispatchEcharts();
    renderLastDispatchCurrentUserEcharts();

    // 弹出框展示已启动定时任务的项目及其cron表达式
    tippyProjectSchedulerInfo();
});

// 元素
let $show_project_scheduler_info = $('#show-project-scheduler-info');

// 近期构建数据
function renderLastDispatchEcharts() {
     // 基于准备好的dom，初始化echarts实例
    let echart = echarts.init(document.getElementById('echarts-line-last-dispatch-count'));

    let option = {
        title: {
            // text: '近期构建次数统计'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            // data: ['构建', '成功', '失败', '错误']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '构建',  // name无须与dataset.dimensions一致，而需要保证series顺序与dataset.dimensions一致
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#007bfe'
                },
            },
            {
                name: '成功',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#28a545'
                },
            },
            {
                name: '失败',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#db3545'
                },
            },
            {
                name: '错误',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#cd00cc'
                },
            },
            {
                name: '中止',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#6c757d'
                },
            },
        ],
        dataset: {
            // 'date' 为x轴数据
            // '构建', '成功', '失败', '错误' 为y轴数据
            dimensions: ['date', '构建', '成功', '失败', '错误', '中止'],  // 必须与source中的key一致
            source: echart_line_last_dispatch_dataset_source,
        },
    };

    echart.setOption(option);

    //根据窗口的大小变动图表
    window.addEventListener("resize", () => echart.resize());
}

// 当前用户近期构建数据
function renderLastDispatchCurrentUserEcharts() {
     // 基于准备好的dom，初始化echarts实例
    let echart = echarts.init(document.getElementById('echarts-line-last-dispatch-count-current-user'));

    let option = {
        title: {
            // text: '当前用户近期构建次数统计'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            // data: ['构建', '成功', '失败', '错误']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '构建',  // name无须与dataset.dimensions一致，而需要保证series顺序与dataset.dimensions一致
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#007bfe'
                },
            },
            {
                name: '成功',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#28a545'
                },
            },
            {
                name: '失败',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#db3545'
                },
            },
            {
                name: '错误',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#cd00cc'
                },
            },
            {
                name: '中止',
                type: 'line',
                smooth: true,
                itemStyle: {
                    color: '#6c757d'
                },
            },
        ],
        dataset: {
            // 'date' 为x轴数据
            // '构建', '成功', '失败', '错误' 为y轴数据
            dimensions: ['date', '构建', '成功', '失败', '错误', '中止'],  // 必须与source中的key一致
            source: echart_line_last_dispatch_current_user_dataset_source,
        },
    };

    echart.setOption(option);

    //根据窗口的大小变动图表
    window.addEventListener("resize", () => echart.resize());
}

// 弹出框展示已启动定时任务的项目及其cron表达式
function tippyProjectSchedulerInfo() {
    tippy($show_project_scheduler_info[0], {
        trigger: 'click',
        hideOnClick: true,
        placement: 'bottom',
        arrow: false,
        allowHTML: true,
        interactive: true,
        content: function (){
            return $show_project_scheduler_info.next().html();
        },
        // appendTo: 'parent',
        appendTo: () => document.body,
        // zIndex默认9999 当有模态框显示时，提示框依然会展示在最上层且可以操作点击，因此更改zIndex为0，将其放置于模态框下层
        // 但如果设置为0时，handsontable表格会和tippy框样式重叠，尝试了多个zIndex数值，发现大于200可以解决重叠问题
        // zIndex: 200,
        theme: 'light',
        maxWidth: 'none',
    });
}