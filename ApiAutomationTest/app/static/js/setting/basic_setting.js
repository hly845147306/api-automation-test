$(document).ready(function () {

    // 表格实例化
    let table_ding_talk_robot_setting = new getDingTalkRobotSettingTable();
    table_ding_talk_robot_setting.init();
    let table_email_receiver_setting = new getEmailReceiverSettingTable();
    table_email_receiver_setting.init();

    // 事件绑定
    $btn_add_ding_talk_robot.on('click', addDingTalkRobotSetting);
    $btn_save_add_ding_talk_robot_setting.on('click', saveAddDingTalkRobotSetting);
    $btn_save_modify_ding_talk_robot_setting.on('click', saveModifyDingTalkRobotSetting);
    $btn_add_email_receiver.on('click', addEmailReceiverSetting);
    $btn_save_add_email_receiver_setting.on('click', saveAddEmailReceiverSetting);
    $btn_save_modify_email_receiver_setting.on('click', saveModifyEmailReceiverSetting);

});

// Dom元素
let $table_ding_talk_robot_setting = $('#table-ding-talk-robot-setting');
let $btn_add_ding_talk_robot = $('#btn-add-ding-talk-robot');
let $modal_add_ding_talk_robot_setting = $('#modal-add-ding-talk-robot-setting');
let $input_add_ding_talk_robot_enable_yes = $('#input-add-ding-talk-robot-enable-yes');
let $input_add_ding_talk_robot_enable_no = $('#input-add-ding-talk-robot-enable-no');
let $input_add_ding_talk_robot_at_all_yes = $('#input-add-ding-talk-robot-at-all-yes');
let $input_add_ding_talk_robot_at_all_no = $('#input-add-ding-talk-robot-at-all-no');
let $input_add_ding_talk_robot_access_token = $('#input-add-ding-talk-robot-access-token');
let $input_add_ding_talk_robot_secret = $('#input-add-ding-talk-robot-secret');
let $input_add_ding_talk_robot_at_mobiles = $('#input-add-ding-talk-robot-at-mobiles');
let $select_add_ding_talk_robot_projects = $('#select-add-ding-talk-robot-projects');
let $btn_save_add_ding_talk_robot_setting = $('#btn-save-add-ding-talk-robot-setting');
let $modal_modify_ding_talk_robot_setting = $('#modal-modify-ding-talk-robot-setting');
let $input_modify_ding_talk_robot_id = $('#input-modify-ding-talk-robot-id');
let $input_modify_ding_talk_robot_enable_yes = $('#input-modify-ding-talk-robot-enable-yes');
let $input_modify_ding_talk_robot_enable_no = $('#input-modify-ding-talk-robot-enable-no');
let $input_modify_ding_talk_robot_at_all_yes = $('#input-modify-ding-talk-robot-at-all-yes');
let $input_modify_ding_talk_robot_at_all_no = $('#input-modify-ding-talk-robot-at-all-no');
let $input_modify_ding_talk_robot_access_token = $('#input-modify-ding-talk-robot-access-token');
let $input_modify_ding_talk_robot_secret = $('#input-modify-ding-talk-robot-secret');
let $input_modify_ding_talk_robot_at_mobiles = $('#input-modify-ding-talk-robot-at-mobiles');
let $select_modify_ding_talk_robot_projects = $('#select-modify-ding-talk-robot-projects');
let $btn_save_modify_ding_talk_robot_setting = $('#btn-save-modify-ding-talk-robot-setting');
let $table_email_receiver_setting = $('#table-email-receiver-setting');
let $btn_add_email_receiver = $('#btn-add-email-receiver');
let $modal_add_email_receiver_setting = $('#modal-add-email-receiver-setting');
let $input_add_email_receiver_enable_yes = $('#input-add-email-receiver-enable-yes');
let $input_add_email_receiver_enable_no = $('#input-add-email-receiver-enable-no');
let $input_add_email_receiver_name = $('#input-add-email-receiver-name');
let $input_add_email_receiver_address = $('#input-add-email-receiver-address');
let $select_add_email_receiver_projects = $('#select-add-email-receiver-projects');
let $btn_save_add_email_receiver_setting = $('#btn-save-add-email-receiver-setting');
let $modal_modify_email_receiver_setting = $('#modal-modify-email-receiver-setting');
let $input_modify_email_receiver_id = $('#input-modify-email-receiver-id');
let $input_modify_email_receiver_enable_yes = $('#input-modify-email-receiver-enable-yes');
let $input_modify_email_receiver_enable_no = $('#input-modify-email-receiver-enable-no');
let $input_modify_email_receiver_name = $('#input-modify-email-receiver-name');
let $input_modify_email_receiver_address = $('#input-modify-email-receiver-address');
let $select_modify_email_receiver_projects = $('#select-modify-email-receiver-projects');
let $btn_save_modify_email_receiver_setting = $('#btn-save-modify-email-receiver-setting');

// 获取钉钉机器人表格对象
function getDingTalkRobotSettingTable() {
    let table = new Object;

    table.init = function () {
        $table_ding_talk_robot_setting.bootstrapTable({
            buttonsClass: 'primary',            // 定义按钮类
            cardView: false,                    // 是否显示详细视图
            cache: false,                       // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性
            // checkboxHeader: false,              // 展示check-all按钮
            classes: 'table table-bordered table-hover table-striped table-borderless',  // 图表样式
            clickToSelect: false,               // 是否启用点击选中行
            detailView: false,                  // 是否显示父子表
            height: 500,                        // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            locale: 'zh-CN',
            method: 'post',                     // 请求方式
            minimumCountColumns: 2,             // 最少允许的列数
            pageNumber:1,                       // 初始化加载第一页，默认第一页
            pagination: true,                   // 是否显示分页
            pageSize: 5,                        // 每页的记录行数
            pageList: [5, 10, 25, 50, 100],     // 可供选择的每页的行数
            paginationLoop: true,               // 循环翻页模式
            // queryParams: table.queryParams,     //传递参数
            search: true,                       // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            // searchHighlight: true,              // bug?
            striped: true,                      // 是否显示行间隔色
            strictSearch: false,                // 严格搜索，非模糊搜素
            showColumns: true,                  // 是否显示所有的列
            showColumnsToggleAll: true,         // 全选开关
            showRefresh: true,                  // 是否显示刷新按钮
            showToggle:true,                    // 是否显示详细视图和列表视图的切换按钮
            sidePagination: 'client',           // 分页方式：client客户端分页，server服务端分页
            // sortable: true,                     //是否启用排序
            sortClass: 'table-active',          // 排序列样式
            // sortOrder: 'asc',                   // 排序方式
            sortReset: true,                    // 重置被排序的列（点击三次）
            toolbar: '#table-ding-talk-robot-setting-toolbar',  // 工具按钮用哪个容器
            toolbarAlign: 'left',               // 工具栏按钮位置
            url: '/ajax/setting/ding_talk_robot_setting/get',           // 请求后台的URL
            uniqueId: 'id',                     // 每一行的唯一标识，一般为主键列
            // virtualScroll: true,
            columns: [
                {
                    field: 'id',
                    title: '编号',
                    widthUnit : '%',
                    width: '5',
                }, {
                    field: 'enable',
                    title: '启用',
                    widthUnit : '%',
                    width: '5',
                    formatter: enableFormatter,
                }, {
                    field: 'access_token',
                    title: 'AccessToken',
                    widthUnit : '%',
                    width: '25',
                    formatter: tooltipFormatter,
                }, {
                    field: 'secret',
                    title: '密钥',
                    widthUnit : '%',
                    width: '25',
                    formatter: tooltipFormatter,
                }, {
                    field: 'at_all',
                    title: '@全体成员',
                    widthUnit : '%',
                    width: '10',
                    formatter: atAllFormatter,
                }, {
                    field: 'at_mobiles',
                    title: '@成员(手机号)',
                    widthUnit : '%',
                    width: '10',
                    formatter: tooltipFormatter,
                }, {
                    field: 'projects',
                    title: '关联项目',
                    widthUnit : '%',
                    width: '10',
                    formatter: tooltipFormatter,
                }, {
                    filed: 'action',
                    title: '操作',
                    widthUnit : '%',
                    width: '15',
                    formatter: actionFormatter,
                    events: 'actionEvents',
                }
            ],
            onLoadSuccess: onLoadSuccess,  // Fires when remote data is loaded successfully
        });
    };

    function actionFormatter(value, row, index, field) {
        return '<button class="btn btn-outline-secondary btn-ding-talk-robot-setting-delete-row mr-2">删除</button>' +
            '<button class="btn btn-outline-secondary btn-ding-talk-robot-setting-modify-row mr-2">修改</button>'
    }

    function tooltipFormatter(value, row, index, field) {
        return `<span data-toggle="tooltip" data-placement="top" title="${value}">${value}</span>`
    }

    function atAllFormatter(value, row, index, field) {
        if(value === true){
            return '是'
        }else{
            return '否'
        }
    }

    function enableFormatter(value, row, index, field) {
        if(value === true){
            return '是'
        }else{
            return '否'
        }
    }

    function onLoadSuccess(data, status, jqXHR){
        // 渲染bootstrap-tooltip
        $('[data-toggle="tooltip"]').tooltip();
    }

    window.actionEvents = {
        'click .btn-ding-talk-robot-setting-delete-row': function (event, value, row, index) {
            Swal.fire({
                title: '确定删除 编号:' + row.id + ' 钉钉机器人吗？',
                text: "删除后将无法恢复!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '确定',
                cancelButtonColor: '#d33',
                cancelButtonText: '取消',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/setting/ding_talk_robot_setting/delete',
                        data: {
                            id: row.id,
                        },
                        success: function (data, status, xhr) {
                            if (data.error_no === 0){
                                message("钉钉机器人删除成功", "success");
                                $table_ding_talk_robot_setting.bootstrapTable('refresh');
                            }else{
                                message("钉钉机器人删除失败: " + data.error_msg, "error");
                            }
                        },
                    });
                }
            });
        },

        'click .btn-ding-talk-robot-setting-modify-row': function (event, value, row, index) {
            $input_modify_ding_talk_robot_id.val(row.id);
            $modal_modify_ding_talk_robot_setting.modal('show');
            if(row.enable){
                $input_modify_ding_talk_robot_enable_yes.prop('checked', true).parent().addClass('active');
                $input_modify_ding_talk_robot_enable_no.prop('checked', false).parent().removeClass('active');
            }else{
                $input_modify_ding_talk_robot_enable_yes.prop('checked', false).parent().removeClass('active');
                $input_modify_ding_talk_robot_enable_no.prop('checked', true).parent().addClass('active');
            }
            if(row.at_all){
                $input_modify_ding_talk_robot_at_all_yes.prop('checked', true).parent().addClass('active');
                $input_modify_ding_talk_robot_at_all_no.prop('checked', false).parent().removeClass('active');
            }else{
                $input_modify_ding_talk_robot_at_all_yes.prop('checked', false).parent().removeClass('active');
                $input_modify_ding_talk_robot_at_all_no.prop('checked', true).parent().addClass('active');
            }
            $input_modify_ding_talk_robot_access_token.val(row.access_token);
            $input_modify_ding_talk_robot_secret.val(row.secret);
            $input_modify_ding_talk_robot_at_mobiles.val(row.at_mobiles);
            $select_modify_ding_talk_robot_projects.selectpicker('val', row.projects);
        },
    };

    return table;
}

// 获取邮件通知表格对象
function getEmailReceiverSettingTable() {
    let table = new Object;

    table.init = function () {
        $table_email_receiver_setting.bootstrapTable({
            buttonsClass: 'primary',            // 定义按钮类
            cardView: false,                    // 是否显示详细视图
            cache: false,                       // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性
            // checkboxHeader: false,              // 展示check-all按钮
            classes: 'table table-bordered table-hover table-striped table-borderless',  // 图表样式
            clickToSelect: false,               // 是否启用点击选中行
            detailView: false,                  // 是否显示父子表
            height: 500,                        // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            locale: 'zh-CN',
            method: 'post',                     // 请求方式
            minimumCountColumns: 2,             // 最少允许的列数
            pageNumber:1,                       // 初始化加载第一页，默认第一页
            pagination: true,                   // 是否显示分页
            pageSize: 5,                        // 每页的记录行数
            pageList: [5, 10, 25, 50, 100],     // 可供选择的每页的行数
            paginationLoop: true,               // 循环翻页模式
            // queryParams: table.queryParams,     //传递参数
            search: true,                       // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            // searchHighlight: true,              // bug?
            striped: true,                      // 是否显示行间隔色
            strictSearch: false,                // 严格搜索，非模糊搜素
            showColumns: true,                  // 是否显示所有的列
            showColumnsToggleAll: true,         // 全选开关
            showRefresh: true,                  // 是否显示刷新按钮
            showToggle:true,                    // 是否显示详细视图和列表视图的切换按钮
            sidePagination: 'client',           // 分页方式：client客户端分页，server服务端分页
            // sortable: true,                     //是否启用排序
            sortClass: 'table-active',          // 排序列样式
            // sortOrder: 'asc',                   // 排序方式
            sortReset: true,                    // 重置被排序的列（点击三次）
            toolbar: '#table-email-receiver-setting-toolbar',  // 工具按钮用哪个容器
            toolbarAlign: 'left',               // 工具栏按钮位置
            url: '/ajax/setting/email_receiver_setting/get',           // 请求后台的URL
            uniqueId: 'id',                     // 每一行的唯一标识，一般为主键列
            // virtualScroll: true,
            columns: [
                {
                    field: 'id',
                    title: '编号',
                    widthUnit : '%',
                    width: '5',
                }, {
                    field: 'enable',
                    title: '启用',
                    widthUnit : '%',
                    width: '5',
                    formatter: enableFormatter,
                }, {
                    field: 'name',
                    title: '名称',
                    widthUnit : '%',
                    width: '25',
                    formatter: tooltipFormatter,
                }, {
                    field: 'address',
                    title: '邮箱地址',
                    widthUnit : '%',
                    width: '25',
                    formatter: tooltipFormatter,
                }, {
                    field: 'projects',
                    title: '关联项目',
                    widthUnit : '%',
                    width: '20',
                    formatter: tooltipFormatter,
                }, {
                    filed: 'action',
                    title: '操作',
                    widthUnit : '%',
                    width: '20',
                    formatter: actionFormatter,
                    events: 'actionEvents',
                }
            ],
            onLoadSuccess: onLoadSuccess,  // Fires when remote data is loaded successfully
        });
    };

    function actionFormatter(value, row, index, field) {
        return '<button class="btn btn-outline-secondary btn-email-receiver-setting-delete-row mr-2">删除</button>' +
            '<button class="btn btn-outline-secondary btn-email-receiver-setting-modify-row mr-2">修改</button>'
    }

    function tooltipFormatter(value, row, index, field) {
        return `<span data-toggle="tooltip" data-placement="top" title="${value}">${value}</span>`
    }

    function enableFormatter(value, row, index, field) {
        if(value === true){
            return '是'
        }else{
            return '否'
        }
    }

    function onLoadSuccess(data, status, jqXHR){
        // 渲染bootstrap-tooltip
        $('[data-toggle="tooltip"]').tooltip();
    }

    window.actionEvents['click .btn-email-receiver-setting-delete-row'] = function (event, value, row, index) {
        Swal.fire({
            title: '确定删除 名称:' + row.name + ' 收件人吗？',
            text: "删除后将无法恢复!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: '确定',
            cancelButtonColor: '#d33',
            cancelButtonText: '取消',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: '/ajax/setting/email_receiver_setting/delete',
                    data: {
                        id: row.id,
                    },
                    success: function (data, status, xhr) {
                        if (data.error_no === 0){
                            message("邮件收件人删除成功", "success");
                            $table_email_receiver_setting.bootstrapTable('refresh');
                        }else{
                            message("邮件收件人删除失败: " + data.error_msg, "error");
                        }
                    },
                });
            }
        });
    };

    window.actionEvents['click .btn-email-receiver-setting-modify-row'] = function (event, value, row, index) {
        console.log(row);
        $input_modify_email_receiver_id.val(row.id);
        $modal_modify_email_receiver_setting.modal('show');
        if(row.enable){
            $input_modify_email_receiver_enable_yes.prop('checked', true).parent().addClass('active');
            $input_modify_email_receiver_enable_no.prop('checked', false).parent().removeClass('active');
        }else{
            $input_modify_email_receiver_enable_yes.prop('checked', false).parent().removeClass('active');
            $input_modify_email_receiver_enable_no.prop('checked', true).parent().addClass('active');
        }
        $input_modify_email_receiver_name.val(row.name);
        $input_modify_email_receiver_address.val(row.address);
        $select_modify_email_receiver_projects.selectpicker('val', row.projects);
    };

    return table;
}

// 打开新增钉钉机器人模态对话框
function addDingTalkRobotSetting(event) {
    $modal_add_ding_talk_robot_setting.modal('show');
}
// 新增钉钉机器人配置
function saveAddDingTalkRobotSetting(event) {
    let enable = false;
    let at_all = false;
    if ($input_add_ding_talk_robot_enable_yes.is(':checked')){
        enable = true;
    }
    if ($input_add_ding_talk_robot_at_all_yes.is(':checked')){
        at_all = true;
    }
    let projects = $select_add_ding_talk_robot_projects.val();
    let access_token = $input_add_ding_talk_robot_access_token.val();
    let secret = $input_add_ding_talk_robot_secret.val();
    let at_mobiles = $input_add_ding_talk_robot_at_mobiles.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/setting/ding_talk_robot_setting/add',
        data: {
            enable: enable,
            at_all: at_all,
            projects: JSON.stringify(projects),
            access_token: access_token,
            secret: secret,
            at_mobiles: at_mobiles,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("钉钉机器人新增成功", "success");
                $modal_add_ding_talk_robot_setting.modal('hide');
                $table_ding_talk_robot_setting.bootstrapTable('refresh');
                initAddDingTalkRobotSettingModal();
            }else{
                message("钉钉机器人新增失败: " + data.error_msg, "error");
            }
        },
    });
}
// 修改钉钉机器人配置
function saveModifyDingTalkRobotSetting(event) {
    let enable = false;
    let at_all = false;
    if ($input_modify_ding_talk_robot_enable_yes.is(':checked')){
        enable = true;
    }
    if ($input_modify_ding_talk_robot_at_all_yes.is(':checked')){
        at_all = true;
    }
    let id = $input_modify_ding_talk_robot_id.val();
    let projects = $select_modify_ding_talk_robot_projects.val();
    let access_token = $input_modify_ding_talk_robot_access_token.val();
    let secret = $input_modify_ding_talk_robot_secret.val();
    let at_mobiles = $input_modify_ding_talk_robot_at_mobiles.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/setting/ding_talk_robot_setting/modify',
        data: {
            id: id,
            enable: enable,
            at_all: at_all,
            projects: JSON.stringify(projects),
            access_token: access_token,
            secret: secret,
            at_mobiles: at_mobiles,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("钉钉机器人修改成功", "success");
                $modal_modify_ding_talk_robot_setting.modal('hide');
                $table_ding_talk_robot_setting.bootstrapTable('refresh');
                initModifyDingTalkRobotSettingModal();
            }else{
                message("钉钉机器人修改失败: " + data.error_msg, "error");
            }
        },
    });
}
// 恢复钉钉机器人对话框初始状态
function initAddDingTalkRobotSettingModal() {
    $input_add_ding_talk_robot_enable_yes.prop('checked', false).parent().removeClass('active');
    $input_add_ding_talk_robot_enable_no.prop('checked', true).parent().addClass('active');
    $input_add_ding_talk_robot_at_all_yes.prop('checked', false).parent().removeClass('active');
    $input_add_ding_talk_robot_at_all_no.prop('checked', true).parent().addClass('active');
    $select_add_ding_talk_robot_projects.val('');
    $($select_add_ding_talk_robot_projects).selectpicker('refresh');
    $input_add_ding_talk_robot_access_token.val('');
    $input_add_ding_talk_robot_secret.val('');
    $input_add_ding_talk_robot_at_mobiles.val('');
}
// 恢复钉钉机器人对话框初始状态
function initModifyDingTalkRobotSettingModal() {
    $input_modify_ding_talk_robot_id.val('');
    $input_modify_ding_talk_robot_enable_yes.prop('checked', false).parent().removeClass('active');
    $input_modify_ding_talk_robot_enable_no.prop('checked', true).parent().addClass('active');
    $input_modify_ding_talk_robot_at_all_yes.prop('checked', false).parent().removeClass('active');
    $input_modify_ding_talk_robot_at_all_no.prop('checked', true).parent().addClass('active');
    $select_modify_ding_talk_robot_projects.val('');
    $($select_modify_ding_talk_robot_projects).selectpicker('refresh');
    $input_modify_ding_talk_robot_access_token.val('');
    $input_modify_ding_talk_robot_secret.val('');
    $input_modify_ding_talk_robot_at_mobiles.val('');
}

// 打开新增邮件收件人模态对话框
function addEmailReceiverSetting(event) {
    $modal_add_email_receiver_setting.modal('show');
}
// 新增邮件收件人
function saveAddEmailReceiverSetting(event) {
    let enable = false;
    if ($input_add_email_receiver_enable_yes.is(':checked')){
        enable = true;
    }
    let projects = $select_add_email_receiver_projects.val();
    let name = $input_add_email_receiver_name.val();
    let address = $input_add_email_receiver_address.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/setting/email_receiver_setting/add',
        data: {
            enable: enable,
            projects: JSON.stringify(projects),
            name: name,
            address: address,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("钉钉机器人新增成功", "success");
                $modal_add_email_receiver_setting.modal('hide');
                $table_email_receiver_setting.bootstrapTable('refresh');
                initAddEmailReceiverSettingModal();
            }else{
                message("钉钉机器人新增失败: " + data.error_msg, "error");
            }
        },
    });
}
// 修改邮件收件人
function saveModifyEmailReceiverSetting(event) {
    let enable = false;
    if ($input_modify_email_receiver_enable_yes.is(':checked')){
        enable = true;
    }
    let id = $input_modify_email_receiver_id.val();
    let projects = $select_modify_email_receiver_projects.val();
    let name = $input_modify_email_receiver_name.val();
    let address = $input_modify_email_receiver_address.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/setting/email_receiver_setting/modify',
        data: {
            id: id,
            enable: enable,
            name: name,
            address: address,
            projects: JSON.stringify(projects),
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("邮件收件人修改成功", "success");
                $modal_modify_email_receiver_setting.modal('hide');
                $table_email_receiver_setting.bootstrapTable('refresh');
                initModifyEmailReceiverSettingModal();
            }else{
                message("邮件收件人修改失败: " + data.error_msg, "error");
            }
        },
    });
}
// 恢复邮件收件人对话框初始状态
function initAddEmailReceiverSettingModal() {
    $input_add_email_receiver_enable_yes.prop('checked', false).parent().removeClass('active');
    $input_add_email_receiver_enable_no.prop('checked', true).parent().addClass('active');
    $select_add_email_receiver_projects.val('');
    $($select_add_email_receiver_projects).selectpicker('refresh');
    $input_add_email_receiver_name.val('');
    $input_add_email_receiver_address.val('');
}
// 恢复邮件收件人对话框初始状态
function initModifyEmailReceiverSettingModal() {
    $input_modify_email_receiver_id.val('');
    $input_modify_email_receiver_enable_yes.prop('checked', false).parent().removeClass('active');
    $input_modify_email_receiver_enable_no.prop('checked', true).parent().addClass('active');
    $select_modify_email_receiver_projects.val('');
    $($select_modify_email_receiver_projects).selectpicker('refresh');
    $input_modify_email_receiver_name.val('');
    $input_modify_email_receiver_address.val('');
}
