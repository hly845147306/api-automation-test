# coding=utf-8

import os

app_dir = os.path.dirname(os.path.realpath(__file__))
proj_dir = os.path.dirname(app_dir)
attachment_dir = os.path.join(proj_dir, 'attachment')


class Config:

    # 版本编号
    AAT_VERSION = '0.3.1'  # 'AAT' is 'ApiAutomationTest'

    # SERVER_NAME = 'WebUiAutoTest:5000'  # enable subdomain support
    # SERVER_NAME = '0.0.0.0:5000'  # enable subdomain support

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    # Flask-SQLAlchemy插件从SQLALCHEMY_DATABASE_URI配置变量中获取应用的数据库的位置
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(app_dir, 'app.db')
    # SQLALCHEMY_TRACK_MODIFICATIONS配置项用于设置数据发生变更之后是否发送信号给应用，不需要这项功能，因此将其设置为False。
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # flask_mail 配置
    MAIL_SERVER = 'smtp-mail.outlook.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = '********'
    MAIL_PASSWORD = '********'
    MAIL_DEFAULT_SENDER = ('Admin', MAIL_USERNAME)
    ADMINS = [MAIL_USERNAME]

    # CSRF
    WTF_CSRF_TIME_LIMIT = 60 * 60 * 24  # 24h超时限制
