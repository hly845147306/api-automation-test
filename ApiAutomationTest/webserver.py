# coding=utf-8

import logging

from app import create_app

from app.extensions import socketio


app = create_app()

if __name__ == '__main__':
    host, port = '0.0.0.0', 5000
    debug = False
    log_output = True
    if socketio.async_mode == 'threading':
        socketio.run(app=app, host=host, port=port, debug=debug)
    elif socketio.async_mode == 'eventlet':
        socketio.run(app=app, host=host, port=port, debug=debug, log_output=log_output, log=logging.getLogger())
    elif socketio.async_mode == 'gevent':
        socketio.run(app=app, host=host, port=port, debug=debug, log_output=log_output)
    # app.run(host='0.0.0.0', port=5000)
    # app.run()
