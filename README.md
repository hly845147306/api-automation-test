# ApiAutomationTest

[![Python Version](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release)
[![License](https://img.shields.io/:license-apache-brightgreen.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

接口自动化测试平台`ApiAutomationTest`

- 使用她可以像`PostMan`一样进行单个或多个接口调试，或像`JMeter`一样使用逻辑控制器组件对案例步骤进行流程控制。
- 测试案例当然少不了期望断言的设置，她可以方便的为请求接口设置期望断言和脚本断言。
- 她同时也提供了例如定时器、变量定义、脚本等工具组件，您可以使用不同类型的组件编写出丰富测试案例场景。
- 您还可以为项目配置定时任务来周期或定时触发执行。
- 案例执行完毕后查看报告，支持执行结果钉钉通知。

> [Demo示例](http://81.70.247.60:5000/) (用户名/密码 admin/admin)

## 内容列表

- [背景](#背景)
- [软件架构](#软件架构)
- [安装](#安装)
- [使用说明](#使用说明)
    - [页面介绍](#页面介绍)
    - [案例编写](#案例编写)
    - [构建执行](#构建执行)
    - [查看报告](#查看报告)
- [组件说明](#组件说明)
- [维护者](#维护者)
- [鸣谢](#鸣谢)
- [如何贡献](#如何贡献)
- [使用许可](#使用许可)

## 背景

我和大多数测试人员一样，手工接口测试常用`PostMan`来完成。
接口自动化会使用`JMeter+Ant+Jenkins`作为解决方案，实现自动化执行、输出报告
以及定时构建和通知。  
而对于初次接触`JMeter+Ant+Jenkins`自动化解决方案的人来说，搭建跑起来还是有点上手难度的。  
本项目借鉴各个工具特点初步形成了目前主要功能（支持接口手工调试、项目级模块级执行、报告输出、钉钉通知、定时构建）

## 软件架构

- 前端 Bootstrap4 + JS&JQuery
- 后端 Python + Flask
- 依赖库(后端核心第三方库)
    - Flask
    - Flask-SQLAlchemy
    - Flask-Migrate
    - Flask-SocketIO
    - requests
    - PyMySQL
    - paramiko
    - APScheduler

## 安装

开发环境和生产环境使用的是Python3.6版本，理论上>=py3.6都可以，建议大家使用`Python3.6`版本。  
支持`Windows`和`Linux`部署

- 第一步：创建项目虚拟环境  
    进入项目目录`ApiAutomationTest`，在`ApiAutomationTest`目录中执行如下命令创建初始虚拟环境`venv`
    ```
    python -m venv ./venv
    ```

   **重点强调: 后面步骤需使用venv中的执行文件来执行**
    ```
    win
    ./venv/Scripts/pip.exe
    ./venv/Scripts/python.exe
    ./venv/Scripts/flask.exe
    linux
    ./venv/bin/pip
    ./venv/bin/python
    ./venv/bin/flask
    ```

- 第二步: 安装Python第三方库  
    使用pip工具安装
    ```
    pip install -r requirements.txt
    ```
    国内可以使用douban的镜像源快速下载
    ```
    pip install -r requirements.txt -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
    ```

- 第三步: 初始化数据库  
    执行命令
    ```
    flask init
    ```
    
- 第四步: 启动服务  
    执行命令
    ```
    python webserver.py
    ```

## 使用说明

### 页面介绍

- 登录页面  
    初始化数据库后使用默认用户名admin, 密码admin登录
    ![](./docs/auth.login.png)

- 总览页面  
    查看构建信息
    ![](./docs/index.png)

- 项目页面  
    查看并管理所有项目
    ![](./docs/project.png)

- 模块页面  
    查看并管理单个项目中所有模块
    ![](./docs/module.png)

- 案例场景页面  
    编写/调试测试用例(后面重点介绍: [跳转查看](#案例编写))
    ![](./docs/scene.png)

- 报告页面(后面重点介绍: [跳转查看](#查看报告))  
    查看所有报告
    ![](./docs/report.png)

    查看报告详细数据
    ![](./docs/report_detail.png)

- 设置页面  
    设置钉钉通知
    ![](./docs/dingtalk_robot_setting.png)  
    > 配置钉钉机器人时安全设置必须勾选`加签`，并将生成的密钥填入钉钉通知设置项中  
    ![](./docs/dingtalk_robot_security.png)  

### 案例编写

项目(Project) -> 模块(Module) -> 场景(Scene) -> 组件(Element): 项目往下分为4个层级，案例由不同组件组合而成
![](./docs/add_http_element.gif)

- 进入场景页面  
    ![](./docs/demo_project.png)
    点击进入模块下的场景页面
    
- 添加一个HTTP请求  
    ![](./docs/demo_scene.png)
    空的测试场景，点击左下角按钮添加测试场景
    
    ![](./docs/element.png)
    在场景内添加请求组件，这里添加一个HTTP请求

    ![](./docs/demo_http_element.png)
    填写HTTP相关参数，点击发送按钮测试当前接口。在组件下方可以添加期望断言、预处理脚本、后处理脚本。

    > 预处理脚本(解释: 请求组件执行前执行预处理脚本)  
      后处理脚本(解释: 请求组件执行后执行后处理脚本)

- 执行模块级用例
    ![](./docs/demo_run_module.png)
    编写完一个或多个场景后，执行整个模块。

- 查看报告
    ![](./docs/demo_module_report_detail.png)
    报告页面实时查看运行日志和结果


### 构建执行

共有三种执行方式
![](./docs/three_execution_modes.gif)

1. `请求组件`点击`发送`按钮，如下图。
    ![](./docs/demo_http_element.png)

2. `场景页面`点击`执行测试`按钮，如下图。
    ![](./docs/demo_run_module.png)
    > 场景页面包含了一个模块下所有场景，因此这里执行的是一个模块所有案例。

3. `模块页面`点击`执行测试`按钮，如下图。
    ![](./docs/demo_run_project.png)
    > 模块页面包含了一个项目下所有模块，因此这里执行的是一个项目所有用例。

### 查看报告

执行`请求组件`是不会生成测试报告，而是在当前页面中展示请求应答数据和期望断言结果。  
执行`模块`和`项目`级执行时，会生成对应的测试报告。
![](./docs/view_reports.gif)

- 查看最近5个报告
    ![](./docs/show_report_dropdown.png)

- 测试报告支持实时查看执行结果和日志。  
    查看该案例请求和应答数据，以及设置的期望和断言结果。
    ![](./docs/demo_module_report_detail.png)


### 组件说明

组件类型 | 组件名 | 说明
---- | --- | ---  
请求组件 | HTTP | 支持HTTP/HTTPS协议<br>支持常用方法: GET POST DELETE PUT PATCH HEAD OPTIONS
请求组件 |  SSH | 远程执行命令
请求组件 |  SQL | 支持MySQL数据库
请求组件 |  Debug | 获取执行时上下文中所有定义的变量和值
工具组件 | HTTPHeaderManager | 设置HTTP请求头数据
工具组件 | HTTPCookieManager | 设置HTTPCookie数据
工具组件 | VariableDefinition | 定义变量
工具组件 | Timer | 定时器, 暂停一段时间后继续执行
工具组件 | Script | 支持编写Python脚本, 灵活处理复杂逻辑和数据
逻辑组件 | SIMPLE | 简单控制器, 可用于对组件进行分组管理
逻辑组件 | IF | IF控制器, 填写表达式做是非判断, 只有为True才执行控制器内的组件
逻辑组件 | WHILE | WHILE控制器, 满足条件时持续循环, 否则跳出循环
逻辑组件 | LOOP | LOOP控制器, 设置循环次数, 循环执行控制器中的组件

## 维护者

[@azhengzz](https://gitee.com/azhengzz)

## 鸣谢

 - [JMeter](https://jmeter.apache.org/)
 - [PostMan](https://www.postman.com/)
 - [MeterSphere](https://github.com/metersphere/metersphere)
 - [HttpRunner](https://github.com/httprunner/httprunner)

## 如何贡献

非常欢迎你的加入！[提一个 Issue](https://gitee.com/azhengzz/api-automation-test/issues) 或者提交一个 Pull Request。

项目遵循 [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) 行为规范。

## 使用许可

本项目使用 [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) 许可。

## 其他
> 作者联系方式
> - 邮箱: zhangzheng527@outlook.com
> - QQ交流群: 977134581
>
> 相关项目
> - [WebUIAutoTest](https://gitee.com/azhengzz/WebUIAutoTest) (WebUI自动化平台) 
